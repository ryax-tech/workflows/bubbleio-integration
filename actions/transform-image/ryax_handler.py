# Copyright (C) Ryax Technologies.
# This Source Code Form is subject to the terms of the Mozilla Public.
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
import os.path

from PIL import Image


def handle(req):
    image_filename = req["image_in"]
    transformation = req["transformation"]

    # Create image in the save folder as input, append transformation keyword before the filename
    image_in_folder = os.path.dirname(image_filename)
    image_in_filename = os.path.basename(image_filename)
    image_out_filename = os.path.join(image_in_folder, f"{transformation}-{image_in_filename}")

    print(f"Working on image {image_in_filename}")
    print(f"Applying transformation {transformation}")

    with Image.open(image_filename) as im:
        if transformation == "rotate":
            im.rotate(45).save(image_out_filename)
        elif transformation == "grayscale":
            im.convert('LA').save(image_out_filename)
        elif transformation == "resize":
            im.resize((250,250),Image.Resampling.NEAREST).save(image_out_filename)

    print(f"====> Result file saved in  {image_out_filename}")

    return {
        "image_out" : image_out_filename,
    }


if __name__ == "__main__":
    print(
        handle(
            {
                "image_in": "logo.png",
                "transformation": "resize",
            }
        )
    )
