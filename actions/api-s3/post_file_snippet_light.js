api_file_id = "files_to_add" // this must match the paramter on the api for the upload call
url = "https://app.ryax.io/user-api/27cb19ae-6d8f-498a-827b-1b074ecbd97a/e56c06f8-bf63-48cc-9b2b-6cf1acf6ef3f/upload/todo"

function uploadFile(url, fileToUploadList) {
    let xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    const formData = new FormData();
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');
    for(var file=0; file < fileToUploadList.length; file++) {
        formData.append(api_file_id, fileToUploadList[file]);
    }
    xhr.timeout = 2000;
    xhr.send(formData);
}

fileInputId = "fileToUpload" //this must match the id of the input tag on the bubble html
const uploadFileInput = document.getElementById(fileInputId);
uploadFile(url, uploadFileInput.files)
