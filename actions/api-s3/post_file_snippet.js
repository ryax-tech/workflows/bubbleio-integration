api_file_id = "files_to_add" // this must match the paramter on the api for the upload call
url = "https://app.ryax.io/user-api/27cb19ae-6d8f-498a-827b-1b074ecbd97a/e56c06f8-bf63-48cc-9b2b-6cf1acf6ef3f/upload/todo"

function uploadFile(url, fileToUploadList) {
    // Create and configure request object
    let xhr = new XMLHttpRequest();
    xhr.onerror = function (e) {
        bubble_fn_showAlert("Error sending file")
    };
    xhr.ontimeout = function (e) {
        bubble_fn_showAlert("The uploaded timeout, is the file too big?")
    };
    xhr.onload = function (e) {
        if (xhr.readyState == 4 && xhr.status == 200) {
            bubble_fn_showAlert("File sent succesfully...");
        }
    };


    // Format data to send multiple files
    xhr.open("POST", url, true);
    const formData = new FormData();
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');
    for(var file=0; file < fileToUploadList.length; file++) {
        console.log(fileToUploadList[file].name)
        console.log(fileToUploadList[file].size)
        console.log(fileToUploadList[file].type)
        formData.append(api_file_id, fileToUploadList[file]);
    }

    // Send data
    xhr.timeout = 2000;
    xhr.send(formData);
}

fileInputId = "fileToUpload" //this must match the id of the input tag on the bubble html
const uploadFileInput = document.getElementById(fileInputId);
if (uploadFileInput.files.length == 0){
    bubble_fn_showAlert("No files to upload! First choose a file below.")
}else{
    uploadFile(url, uploadFileInput.files)
}
