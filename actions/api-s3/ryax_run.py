# Copyright (C) Ryax Technologies.
# This Source Code Form is subject to the terms of the Mozilla Public.
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
import uvicorn
from fastapi import FastAPI, UploadFile
import os
import asyncio
import boto3
from urllib.parse import quote
from fastapi.responses import FileResponse
from starlette.middleware.cors import CORSMiddleware
import json


def connect_to_bucket(name: str, access_key: str, secret_key: str):
    s3 = boto3.resource(
        "s3", aws_access_key_id=access_key, aws_secret_access_key=secret_key
    )
    return s3.Bucket(name)

async def run(service, input_values: dict) -> None:
    access_key: str = input_values.get("aws_access_key_id")
    secret_key: str = input_values.get("aws_secret_access_key")
    bucket_name: str = input_values.get("bucket")

    app = FastAPI()

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    my_bucket = connect_to_bucket(bucket_name, access_key, secret_key)

    @app.get(input_values["ryax_endpoint_prefix"]+"/get/{folder_name}/{file_name}")
    async def download_image(folder_name: str, file_name: str):
        print(f"Downloading file {folder_name}/{file_name} from S3...")
        remote_file = os.path.join(folder_name, file_name)
        local_file = os.path.join("/tmp", file_name)
        my_bucket.download_file(remote_file, local_file)
        return FileResponse(local_file)

    @app.get(input_values["ryax_endpoint_prefix"]+"/list/{folder_name}")
    async def list_files_in_folder(folder_name: str):
        print(f"Listing files on S3 bucket {bucket_name}/{folder_name}...")
        all_objs = my_bucket.objects.filter(Prefix=f"{folder_name}")
        files_list = []
        for obj in all_objs:
            if obj.key[-1] == "/":
                continue # skip folder names
            files_list.append({
                "name": obj.key.strip(f"{folder_name}/"),
                "image": input_values["instance_prefix"]+quote(input_values["ryax_endpoint_prefix"]+f"/get/{obj.key}"),
            })
        return files_list

    @app.post(input_values["ryax_endpoint_prefix"]+"/upload/{folder_name}")
    async def upload_file(folder_name: str, files_to_add: list[UploadFile]):
        print("Uploading files to S3...")
        for f in files_to_add:
            print(f"  ===> Uploading {f.filename} of type {f.content_type}")
            temp_filename = os.path.join("/tmp", f.filename)
            content = await f.read()
            with open(temp_filename, "wb") as temp_f:
                temp_f.write(content)
            my_bucket.upload_file(temp_filename, os.path.join(folder_name, f.filename))
            # Launch execution with passthrough parameters
            await service.create_run({
                "aws_access_key_id": input_values.get("aws_access_key_id"),
                "aws_secret_access_key": input_values.get("aws_secret_access_key"),
                "bucket": input_values.get("bucket"),
                "folder": folder_name,
                "uploaded_image": temp_filename,
             })
        print("Files uploaded successfully!")
        return "SUCCESS"

    config = uvicorn.Config(
        app,
        host="0.0.0.0",
        log_level="debug",
        # Default the port and prefix are provided by Ryax in these two entries
        port=input_values["ryax_endpoint_port"],
    )
    server = uvicorn.Server(config)
    await server.serve()

# Used for testing only
if __name__ == "__main__":
    from unittest.mock import AsyncMock

    with open("secrets.txt", "r") as secrets_file:
        secrets = json.load(secrets_file)

    input_json = {
        "ryax_endpoint_prefix": "",
        "ryax_endpoint_port": 8080,
        "aws_access_key_id": "",
        "aws_secret_access_key": "",
        "bucket": "",
        "instance_prefix": "",
    }

    # populate with secrets if any
    for k in input_json:
        if k in secrets:
            input_json[k] = secrets[k]

    asyncio.run(run(AsyncMock(), input_json))