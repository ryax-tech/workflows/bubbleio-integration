# Copyright (C) Ryax Technologies.
# This Source Code Form is subject to the terms of the Mozilla Public.
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
import os.path

from PIL import Image
import requests
import os

def handle(req):
    image_filename = req["image_in"]
    bubble_url = req["bubble_url"]

    with Image.open(image_filename) as im:
        image_type = im.format

    bubble_json = {
        "name": image_filename,
        "type": image_type,
        "size": os.path.getsize(image_filename)
    }

    print(f"Logging {bubble_json}")

    requests.post(bubble_url, json=bubble_json)

    return {}



if __name__ == "__main__":
    print(
        handle(
            {
                "image_in": "logo.png",
                "bubble_url": "https://ryax-image-transform.bubbleapps.io/version-test/api/1.1/obj/imagelog",
            }
        )
    )
