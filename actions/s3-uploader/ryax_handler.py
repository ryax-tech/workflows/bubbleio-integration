#!/usr/bin/env python3
# Copyright (C) Ryax Technologies
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import datetime
from pathlib import Path

import boto3
import os
import json

def create_bucket_key(file_name, timestamp: bool):
    file_name = Path(file_name)
    if timestamp:
        date = datetime.datetime.now().isoformat()
        return file_name.stem + "_" + date + file_name.suffix
    return file_name.stem + file_name.suffix


def connect_to_bucket(name: str, access_key: str, secret_key: str):
    print("Connecting to S3...")
    s3 = boto3.resource(
        "s3", aws_access_key_id=access_key, aws_secret_access_key=secret_key
    )
    print("S3 connection established")
    return s3.Bucket(name)


def handle(req):
    access_key: str = req.get("aws_access_key_id")
    secret_key: str = req.get("aws_secret_access_key")
    bucket_name: str = req.get("bucket")
    file_name: str = req.get("file_to_upload")
    timestamp_bool: bool = (
        True if req.get("timestamp_bool").lower() == "timestamp" else False
    )

    my_bucket = connect_to_bucket(bucket_name, access_key, secret_key)
    file_upload_key = create_bucket_key(file_name, timestamp_bool)
    print(f"Sending {file_name} to {os.path.join(req['folder'], file_upload_key)} at bucket {bucket_name}...")
    my_bucket.upload_file(file_name, os.path.join(req["folder"], file_upload_key))
    return {}

if __name__ == "__main__":
    with open("secrets.txt", "r") as secrets_file:
        secrets = json.load(secrets_file)

    input_json = {
        "file_to_upload": "logo.png",
        "aws_access_key_id": "",
        "aws_secret_access_key": "",
        "bucket": "",
        "folder": "done",
        "timestamp_bool": "timestamp",
    }

    # populate with secrets if any
    for k in input_json:
        if k in secrets:
            input_json[k] = secrets[k]

    print(handle(input_json))
